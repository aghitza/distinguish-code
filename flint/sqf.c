#include <stdlib.h>
#include <stdio.h>
#include <mpir.h>

#include "flint.h"
#include "fmpz.h"
#include "fmpz_mat.h"
#include "fmpz_poly.h"



int main(int argc, char* argv[])
{
    char *filename;
    char line[80];
    FILE *in;
    fmpz_mat_t A;
    fmpz_poly_t f;
    long res, rows, cols;

    if (argc == 2) {
        filename = argv[1];
    } else {
        printf("Syntax: ./sqf <filename>\n");
        return EXIT_FAILURE;
    }
    in = fopen(filename, "r");
    fgets(line, 80, in);
    sscanf(line, "%ld %ld", &rows, &cols);
    fclose(in);
    fmpz_mat_init(A, rows, cols);
    in = fopen(filename, "r");
    fmpz_mat_fread(in, A);
    fclose(in);
    fmpz_mat_print_pretty(A);
    printf("\n");
    fmpz_poly_init(f);
    fmpz_mat_charpoly(f, A);
    fmpz_mat_clear(A);
    fmpz_poly_print_pretty(f, "x");
    res = fmpz_poly_is_squarefree(f);
    fmpz_poly_clear(f);
    printf("\n%ld\n", res);
    return res;
}

