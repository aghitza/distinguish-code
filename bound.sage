from gc import collect


def shrink(S, q):
    P = prime_range(q+1)
    U1 = U2 = []
    FIRST = 1
    for l in P:
        T = S.hecke_operator(l)
        v = list(S.hecke_matrix(l).fcp())
        Lv = len(v)
        if FIRST == 1:
            for j in range(Lv):
                if v[j][1] > 1:
                    M = kernel(v[j][0](T))
                    U2.append(M)
            FIRST = FIRST - 1
        else:
            LU = len(U1)
            for j in range(Lv):
                if v[j][1] > 1:
                    M = kernel(v[j][0](T))
                    for k in range(LU):
                        R = U1[k].intersection(M)
                        d = R.dimension()
                        if d > 1:
                            U2.append(R)
        if len(U2) == 0:
            return([])
        U1 = U2
        U2 = []
    return(U1)


def answer(S, R):
    d = S.dimension()
    if d < 2:
        return(0)                # none needed to distinguish, since normalised
    P = prime_range(R+1)         # initial bound
    if len(P) == 0:
        return(0)
    U1 = U2 = []
    FIRST = 1
    for l in P:
        matrix = S.hecke_matrix(l)
        ExtendedMatrix = matrix.base_extend(QQbar)
        blueprints = ExtendedMatrix.decomposition()
        homes = []
        Lh0 = len(blueprints)
        for h in range(Lh0):
            H = blueprints[h]
            if H[1] is False:               # Ignore dimension 1 bits
                homes.append(H[0])          # No longer need true / false bit
        Lh = len(homes)
        if FIRST == 1:
            U2 = homes
            FIRST = FIRST - 1
        else:
            LU = len(U1)
            for h in range(Lh):
                H = homes[h]
                for k in range(LU):
                    R = U1[k].intersection(H)
                    d = R.dimension()
                    if d > 1:
                        U2.append(R)
        L = len(U2)
        if L == 0:
            return(l)
        U1 = U2
        U2 = []
    return(9999)            # This signals a failure to beat the initial bound


def F(N):                                   # This is [Gamma_0(1): Gamma_0(N)]
    g = 1
    for p in prime_range(2, N+1):
        if N % p == 0:
            g = g*(1+1/p)
    return(g*N)


def calc(N, k, q):
    S = ModularSymbols(N, k, sign=+1).cuspidal_submodule().new_submodule()
    U = shrink(S, q)
    collect()
    L = len(U)
    if L == 0:
        return(q, 1)            # q is an upper bound
    R = k / 12 * F(N)                         # Sturm
    M = -1
    for u in range(L):
        Q = answer(U[u], R)
        collect()
        if Q > M:
            M = Q
    if M < q:
        return(q, 1)             # q is an upper bound
    else:
        return(M, 0)             # M is an exact answer


def solve(N, k):                 # return n_0(N,k)
    S = ModularSymbols(N, k, sign=+1)
    collect()
    S = S.cuspidal_submodule()
    S = S.new_submodule()
    collect()
    d = S.dimension()
    if d < 2:
        return(0)
    U = shrink(S, 7)
    collect()
    L = len(U)
    if L > 0:
        R = k / 12 * F(N)                         # Sturm
        M = -1
        for u in range(L):
            Q = answer(U[u], R)
            collect()
            if Q > M:
                M = Q
        if M >= 7:
            return(M)                      # an unconditional result
    U = shrink(S, 5)                        # answer is at most 7
    collect()
    L = len(U)
    if L > 0:
        R = k / 12 * F(N)                         # Sturm
        M = -1
        for u in range(L):
            Q = answer(U[u], R)
            collect()
            if Q == 7:
                return(7)
            if Q > M:
                M = Q
        if M >= 5:
            return(M)                      # an unconditional result
    U = shrink(S, 3)                        # answer is at most 5
    collect()
    L = len(U)
    if L > 0:
        R = k / 12 * F(N)                         # Sturm
        M = -1
        for u in range(L):
            Q = answer(U[u], R)
            collect()
            if Q == 5:
                return(5)
            if Q > M:
                M = Q
        if M >= 3:
            return(M)                      # an unconditional result
    U = shrink(S, 2)                        # answer is at most 3
    collect()
    L = len(U)
    if L == 0:
        return(2)
    if L > 0:
        R = k / 12 * F(N)                         # Sturm
        M = -1
        for u in range(L):
            Q = answer(U[u], R)
            collect()
            if Q == 3:
                return(3)
            if Q > M:
                M = Q
        if M >= 2:
            return(M)                      # an unconditional result


def k0(N):
    k = 2
    s0 = solve(N, k)
    slst = [s0]
    count = 0
    while True:
        k = k + 2
        s = solve(N, k)
        slst.append(s)
        if s == s0:
            count = count + 1
            if count == 2:
                return (k - 2*2, slst)
        else:
            count = 0
            s0 = s
