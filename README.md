This repository contains the Sage source code used in the preparation of
the paper [Distinguishing newforms][arxiv] by Sam Chow and
Alexandru Ghitza.  It also contains all the data computed using this
code.

The code has been tested with [Sage 6.2][sage].  The main function is `solve(N,
k)` from `bound.sage`.  To use it, download `bound.sage`, start up
Sage and run

    sage: %attach bound.sage
    sage: %time solve(1499, 2)

You should see the following output:

    2
    CPU time: 7.19 s, Wall time: 7.42 s

If you are using [SageMathCloud][smc], create a new project and add the file
`bound.sage` to it; create a new Sage worksheet containing

    load("bound.sage")
    %time solve(1499, 2)

Upon evaluating this, you should see the output:

    2
    CPU time: 7.19 s, Wall time: 7.42 s

[arxiv]: http://arxiv.org/abs/1404.4508
[sage]: http://www.sagemath.org
[smc]: http://cloud.sagemath.com