# Functions for computing the ingredients in Greg Martin's explicit
# formula for the dimension of the space of newforms.
# See G. Martin, "Dimensions of the spaces of cusp forms and newforms on
# Gamma0(N) and Gamma1(N)", Journal of Number Theory 112 (2005),
# 298-331.

def omega(n):                                                # number of divisors
    return(len(prime_divisors(n)))

def splus(n):
    f = factor(n)
    L = len(f)
    ans = 1
    for i in [0..L-1]:
        v = f[i]
        if(v[1] == 1):
            fact = 1-1/v[0]
        elif(v[1] == 2):
            fact = 1-1/v[0]-1/v[0]^2
        else:
            fact = (1-1/v[0])*(1-1/v[0]^2)   
        ans = ans*fact
    return(ans)

def vinf(n):
    f = factor(n)
    L = len(f)
    ans = 1
    for i in [0..L-1]:
        v = f[i]
        if(v[1]%2 != 0):
            fact = 0
        elif(v[1] == 2):
            fact = v[0]-2
        else:
            fact = v[0]^(v[1]/2-2)*(v[0]-1)^2   
        ans = ans*fact
    return(ans)

def ctwo(k):
    ans = 1/4 + (k/4).floor() - k/4
    return(ans)

def cthree(k):
    ans = 1/3 + (k/3).floor() - k/3
    return(ans)

def vtwoplus(n):
    f = factor(n)
    L = len(f)
    ans = 1
    for i in [0..L-1]:
        v = f[i]
        if(v[0] == 2):
            if(v[1] == 1):
                fact = -1
            elif(v[1] == 2):
                fact = -1
            elif(v[1] == 3):
                fact = 1
            else:
                fact = 0
        else:
            if(v[0]%4 == 1):
                if(v[1] == 2):
                    fact = -1
                else:
                    fact = 0
            else:
                if(v[1] == 1):
                    fact = -2
                elif(v[1] == 2):
                    fact = 1
                else:
                    fact = 0
        ans = ans*fact
    return(ans)

def vthreeplus(n):
    f = factor(n)
    L = len(f)
    ans = 1
    for i in [0..L-1]:
        v = f[i]
        if(v[0] == 3):
            if(v[1] == 1):
                fact = -1
            elif(v[1] == 2):
                fact = -1
            elif(v[1] == 3):
                fact = 1
            else:
                fact = 0
        else:
            if(v[0]%3 == 1):
                if(v[1] == 2):
                    fact = -1
                else:
                    fact = 0
            else:
                if(v[1] == 1):
                    fact = -2
                elif(v[1] == 2):
                    fact = 1
                else:
                    fact = 0
        ans = ans*fact
    return(ans)

def gzeroplus(n,k):
    szp = splus(n)
    vip = vinf(n)
    ctw = ctwo(k)
    cth = cthree(k)
    vtw = vtwoplus(n)
    vth = vthreeplus(n)
    ans = (k-1)/12*n*szp - vip/2 + ctw*vtw + cth*vth
    return(ans)

def tee(n):
    P = [x for x in [1..n] if x in Primes()]
    f = factor(n)
    L = len(f)
    ans = 0
    for i in [0..L-1]:
        v = f[i]
        if(v[0] == P[i]):
            ans = ans + 1
        else:
            break
    return(ans)

def compare(n,k):
    S = ModularSymbols(n,k)
    S = S.plus_submodule()
    S = S.cuspidal_submodule()
    S = S.new_submodule()
    d = S.dimension()
    return(d)

def crude(n):
    t = tee(n)
    szp = splus(n)
    vip = vinf(n)
    abvtw = abs(vtwoplus(n))
    abvth = abs(vthreeplus(n))
    rough = 1+12/n/szp*(vip/2+3/4*abvtw+2/3*abvth+2^t)
    ans = 1 + rough.floor()
    return(ans)

for n in [1..999]:
    b = crude(n)
    if(b > 38):
        print(n,b)

def getmax(n):
    m = 2
    for k in [z for z in [1..50] if z%2 == 0]:
        if(gzeroplus(n,k) <= 2^tee(n)):
            m = k +2
    return(m)  

m = 0
r = 0
for n in [2,4,6,12]:
    g = getmax(n)
    if(g > m):
        r = n
        m = g
print(r,m)

